﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cdlity
{
    public class BinaryGap
    {
        public static int solution(int N)
        {
            var bin = Convert.ToString(N, 2);
            int longest = 0, curr = 0;
            bool isSeria = false;
            foreach (var elem in bin)
            {
                if (elem == '1')
                {
                    if (isSeria)
                    {
                        if (longest < curr)
                        {
                            longest = curr;
                            curr = 0;
                            
                        }
                        isSeria = false;
                    }

                }
                else if (elem == '0')
                {
                    if (isSeria)
                    {
                        curr++;
                    }
                    else
                    {
                        curr = 1;
                        isSeria = true;
                    }
                }

            }
            return longest;
        }
    }
}
