﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cdlity
{
    public class ArraysTest
    {
        public static int[] solution1(int[] A, int K)
        {

            int[] B = new int[A.Length];
            Array.Copy(A, B, A.Length);
            int len = A.Length;

            for(int i = 0; i< len; i++)
            {
                B[(K+i)%len] = A[i];
                
            }

            return B;
        }
        public static int solution2(int [] A)
        {
            Dictionary<int, int> dict = new Dictionary<int, int>();
            for(int i =0; i< A.Length; i++)
            {
                if(dict.Keys.Contains(A[i]))
                {
                   
                    dict[A[i]]++;
                }
                else
                {
                    dict[A[i]] = 1;
                }
            }
            return dict.FirstOrDefault(x => x.Value % 2 ==1).Key;

        }
    }
}
